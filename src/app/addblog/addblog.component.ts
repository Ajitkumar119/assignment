import { Component, OnInit } from '@angular/core';
import {AddService} from '../add.service';

@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.css'],
  providers:[AddService]
})
export class AddblogComponent implements OnInit {
post={};
input={};
results=undefined;
  constructor(private add:AddService) { }

  ngOnInit() {
  }
  onpublish(){
    console.log(this.post);
    sessionStorage.setItem('userpost',JSON.stringify(this.post))
  }
  onadd(input){
    this.results=this.add.adddata(input)
    this.input={};
  }
}
