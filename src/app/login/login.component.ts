import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user={};
  constructor(private router : Router) { }

  ngOnInit() {
  }
  onlogin(){
    // this.router.navigateByUrl('home')
    console.log(this.user);
    sessionStorage.setItem('userdata',JSON.stringify(this.user))
  }
}
