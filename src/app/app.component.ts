import { Component } from '@angular/core';
import {Router} from '@angular/router';
import * as firebase from 'firebase';
import firestore from 'firebase/firestore';

const settings = {timestampsInSnapshots: true};
const config = {
  apiKey: 'AIzaSyDsOn9FCIBYSU6W_aKaiSSKe4FkWWKlrbo',
  authDomain: 'userdata-55fdb.firebaseapp.com',
  databaseURL: 'https://userdata-55fdb.firebaseio.comL',
  projectId: 'userdata-55fdb',
  storageBucket: 'userdata-55fdb.appspot.com',
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'assignment';

  constructor( private router:Router) { }

  ngOnInit() {
    this.router.navigateByUrl('')
    firebase.initializeApp(config);
    firebase.firestore().settings(settings);
  }



}
