import { Component, OnInit } from '@angular/core';
import { UserdetailsService} from '../userdetails.service';
import { AddService} from '../add.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[UserdetailsService,AddService]
})
export class HomeComponent implements OnInit {
posts=[];
input={};
post={};
user={};
Employees=[];
result=undefined;
  constructor(private details:UserdetailsService, private add:AddService) { }

  ngOnInit() {
    console.log(sessionStorage);
    this.user=JSON.parse(sessionStorage.userdata)
    console.log(this.user)

    if(sessionStorage.length==0){
      this.posts=[{title:"Bernard shaw", tag:"UI Developer",data:"lorem ipsum"}]
    }else if(sessionStorage.getItem('postarray')!=undefined){
      this.posts=JSON.parse(sessionStorage.postarray) 
      this.post=JSON.parse(sessionStorage.userpost)
      console.log(this.post)
      this.posts.push(this.post)
      console.log(this.posts)
      sessionStorage.setItem('postarray',JSON.stringify(this.posts))
      this.post={};
    }
    else{
      this.posts=[{title:"Bernard shaw", tag:"UI Developer",data:"lorem ipsum"}]
      // this.post=JSON.parse(sessionStorage.userpost)
      this.posts.push(this.post)
      sessionStorage.setItem('postarray',JSON.stringify(this.posts))

    }
  }
  onlogout(){
    sessionStorage.clear();
  }
  onuserclick(){
    this.Employees=this.details.getuserdata()
    console.log(this.Employees)
  }
  onadd(input){
    this.result=this.add.adddata(input)
    this.input={};
  }
}
